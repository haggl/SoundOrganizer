/*
 * Copyright 2012-2014 Sebastian Neuser
 *
 * This file is part of SoundOrganizer.
 *
 * SoundOrganizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SoundOrganizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SoundOrganizer.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.haggl.soundorganizer;

import android.app.Application;
import android.content.Context;


/**
 * An application that to manage haggls sounds.
 * @author haggl
 */
public class SoundOrganizerApplication extends Application
{
    // global build options
    public static final boolean DEBUG = false;

    // file paths
    public static final String exportFileName = "sound-organizer-export";
    public static final String importFileName = "sound-organizer";

    private static Context context;

    /**
     * @return the application context
     */
    public static Context getAppContext()
    {
        return context;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();

        // store reference to the application context
        context = getApplicationContext();
    }
}
