/*
 * Copyright 2012-2014 Sebastian Neuser
 *
 * This file is part of SoundOrganizer.
 *
 * SoundOrganizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SoundOrganizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SoundOrganizer.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.haggl.soundorganizer;

import org.haggl.soundorganizer.adapter.DeviceListAdapter;
import org.haggl.soundorganizer.data.Song;
import org.haggl.soundorganizer.database.DatabaseAssistant;

import android.app.ExpandableListActivity;
import android.content.Intent;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.GestureOverlayView.OnGesturePerformedListener;
import android.gesture.Prediction;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.TextView;


/**
 * Displays the effects setup of a songs.
 * @author haggl
 */
public class SongActivity extends ExpandableListActivity implements OnGesturePerformedListener
{
    // DEBUG variables
    protected static final String TAG = SongActivity.class.getSimpleName();

    // state variables
    private DatabaseAssistant assistant;
    private Song song;
    private GestureLibrary gestures;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize UI
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.song_layout);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Fetch song data
        assistant = DatabaseAssistant.getInstance();
        assistant.open();
        song = assistant.readSong(getIntent().getExtras().getLong("songID"));

        // Load gestures and initialize gesture listener
        gestures = GestureLibraries.fromRawResource(this, R.raw.gestures);
        if (!gestures.load()) {
            finish();
        }
        GestureOverlayView gestures = (GestureOverlayView) findViewById(R.id.gestureOverlay);
        gestures.addOnGesturePerformedListener(this);

        // Fill the view with data
        ((TextView) findViewById(R.id.song_title)).setText(song.getTitle());
        setListAdapter(new DeviceListAdapter((ExpandableListView) findViewById(android.R.id.list), song.getID(), this));
    }

    @Override
    public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture) {
        for (Prediction currentPrediction : gestures.recognize( gesture )) {
            if (currentPrediction.score > 1.0) {
                // Ignore gestures on unlisted songs
                if (song.getNumber() == null) {
                    return;
                }

                // Compute new song number
                int songNumber = song.getNumber();
                if (currentPrediction.name.equals( "left" )) {
                    --songNumber;
                }
                else {
                    ++songNumber;
                }

                // Abort if the song is withing the boundaries of the set list
                if (songNumber < 1 || songNumber > assistant.countSongsWithNumbers()) {
                    return;
                }

                // Start a new activity and provide it with the new song ID
                Intent nextIntent = new Intent(getApplicationContext(), SongActivity.class);
                nextIntent.putExtra("songID", assistant.findIdForSongWithNumber((short) (songNumber)));
                startActivity(nextIntent);
                overridePendingTransition(0, 0);
                finish();
            }
        }
    }
}