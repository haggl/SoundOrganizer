/*
 * Copyright 2012-2014 Sebastian Neuser
 *
 * This file is part of SoundOrganizer.
 *
 * SoundOrganizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SoundOrganizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SoundOrganizer.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.haggl.soundorganizer.data;

/**
 * Represents a parameter of a device.
 *
 * @author haggl
 */
public class Setting
{
    private Long id;
    private Long songID;
    private Long deviceID;
    private String name;
    private String value;

    /**
     * @param name
     * @param value
     */
    public Setting( Long id, Long sondID, Long deviceID, String name, String value )
    {
        super();
        this.id = id;
        this.songID = sondID;
        this.deviceID = deviceID;
        this.name = name;
        this.value = value;
    }

    /**
     * @return the deviceID
     */
    public Long getDeviceID()
    {
        return deviceID;
    }

    /**
     * @return the id
     */
    public Long getID()
    {
        return id;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }
    /**
     * @return the songID
     */
    public Long getSongID()
    {
        return songID;
    }

    /**
     * @return the value
     */
    public String getValue()
    {
        return value;
    }

    /**
     * @param deviceID the deviceID to set
     */
    public void setDeviceID( Long deviceID )
    {
        this.deviceID = deviceID;
    }

    /**
     * @param id the id to set
     */
    public void setID( Long id )
    {
        this.id = id;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName( String name )
    {
        this.name = name;
    }

    /**
     * @param songID the songID to set
     */
    public void setSongID( Long songID )
    {
        this.songID = songID;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue( String value )
    {
        this.value = value;
    }

    @Override
    public String toString()
    {
        return name + ":" + value;
    }
}
