/*
 * Copyright 2012-2014 Sebastian Neuser
 *
 * This file is part of SoundOrganizer.
 *
 * SoundOrganizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SoundOrganizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SoundOrganizer.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.haggl.soundorganizer.data;

/**
 * Represents a parameter of a device.
 *
 * @author haggl
 */
public class Parameter
{
    private long id;
    private String name;
    private String range;

    /**
     * @param name
     * @param range
     */
    public Parameter( long id, String name, String range )
    {
        super();
        this.id = id;
        this.name = name;
        this.range = range;
    }

    /**
     * @return the id
     */
    public long getID()
    {
        return id;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @return the range
     */
    public String getRange()
    {
        return range;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName( String name )
    {
        this.name = name;
    }

    /**
     * @param range
     *            the range to set
     */
    public void setRange( String range )
    {
        this.range = range;
    }

    @Override
    public String toString()
    {
        return name + ":" + range;
    }
}
