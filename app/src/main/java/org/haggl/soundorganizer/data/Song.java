/*
 * Copyright 2012-2014 Sebastian Neuser
 *
 * This file is part of SoundOrganizer.
 *
 * SoundOrganizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SoundOrganizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SoundOrganizer.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.haggl.soundorganizer.data;


/**
 * Represents a song.
 * @author haggl
 */
public class Song implements Comparable<Song>
{
    private long id;
    private String title;
    private Short number;
    private Short length;

    /**
     * @param id
     * @param title
     * @param number
     * @param length
     */
    public Song( long id, String title, Short number, Short length )
    {
        super();
        this.id = id;
        this.title = title;
        this.number = number;
        this.length = length;
    }

    @Override
    public int compareTo( Song another )
    {
        if (number == null && another.number == null) return title.compareTo( another.getTitle() );

        // move songs that are not in the setlist to the end
        if (number == null) return 1;
        if (another.number == null) return -1;

        else return number - another.getNumber();
    }

    /**
     * @return the id
     */
    public long getID()
    {
        return id;
    }

    /**
     * @return the length
     */
    public Short getLength()
    {
        return length;
    }

    /**
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * @return the number
     */
    public Short getNumber()
    {
        return number;
    }

    /**
     * @param length the length to set
     */
    public void setLength( Short length )
    {
        this.length = length;
    }

    /**
     * @param title the name to set
     */
    public void setTitle( String title )
    {
        this.title = title;
    }

    /**
     * @param number the number to set
     */
    public void setNumber( Short number )
    {
        this.number = number;
    }

    public String toString()
    {
        String result = "";

        if (number == null) result += "[+] ";
        else result += "[" + number + "] ";

        result += title;

        if (length != null) result += " (" + length + ")";

        return result;
    }
}
