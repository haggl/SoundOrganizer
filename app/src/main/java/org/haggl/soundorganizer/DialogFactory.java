/*
 * Copyright 2012-2014 Sebastian Neuser
 *
 * This file is part of SoundOrganizer.
 *
 * SoundOrganizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SoundOrganizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SoundOrganizer.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.haggl.soundorganizer;

import org.haggl.soundorganizer.data.Song;
import org.haggl.soundorganizer.database.DatabaseAssistant;
import org.haggl.soundorganizer.R;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.widget.EditText;

/**
 * A collection of dialogs.
 *
 * @author haggl
 */
public class DialogFactory
{
    private SoundOrganizerActivity activity;

    public DialogFactory( SoundOrganizerActivity activity )
    {
        this.activity = activity;
    }

    public Dialog getAddDialog()
    {
        // define the dismiss listener
        OnDismissListener dismissListener = new OnDismissListener(){
            @Override
            public void onDismiss( DialogInterface dialogInterface )
            {
                // get references
                Dialog dialog = (Dialog) dialogInterface;
                EditText nameInput = (EditText) dialog.findViewById( R.id.name_input );
                EditText numberInput = (EditText) dialog.findViewById( R.id.number_input );
                EditText lengthInput = (EditText) dialog.findViewById( R.id.length_input );

                String songName = nameInput.getText().toString().trim();
                if (songName.length() > 0) // abort if no name was provided
                {
                    Short songNumber;
                    if (numberInput.getText().length() == 0) songNumber = null;
                    else songNumber = Short.parseShort( numberInput.getText().toString().trim() );

                    Short songLength;
                    if (lengthInput.getText().length() == 0) songLength = null;
                    else songLength = Short.parseShort( lengthInput.getText().toString().trim() );

                    // update database and views
                    DatabaseAssistant.getInstance().createSong( songName, songNumber, songLength );
                    activity.updateData();
                }
            }
        };

        return buildSongDialog( "add a new song:", dismissListener );
    }

    public Dialog getEditDialog( final Song song )
    {
        // define dismiss listener
        OnDismissListener dismissListener = new OnDismissListener(){
            @Override
            public void onDismiss( DialogInterface dialogInterface )
            {
                // get references
                Dialog dialog = (Dialog) dialogInterface;
                EditText nameInput = (EditText) dialog.findViewById( R.id.name_input );
                EditText numberInput = (EditText) dialog.findViewById( R.id.number_input );
                EditText lengthInput = (EditText) dialog.findViewById( R.id.length_input );

                // parse input
                String name = nameInput.getText().toString().trim();
                if (name.length() == 0) return; // abort if no name was given

                String numberString = numberInput.getText().toString().trim();
                Short number = null;
                if (numberString.length() > 0) number = Short.parseShort( numberString );

                String lengthString = lengthInput.getText().toString().trim();
                Short length = null;
                if (lengthString.length() > 0) length = Short.parseShort( lengthString );

                // update song, database and view
                song.setTitle( name );
                song.setNumber( number );
                song.setLength( length );
                DatabaseAssistant.getInstance().updateSong( song );
                activity.updateData();
            }
        };

        // generate empty dialog
        Dialog editDialog = buildSongDialog( "edit song details:", dismissListener );

        // fill in initial data
        ((EditText) editDialog.findViewById( R.id.name_input )).setText( song.getTitle() );
        if (song.getNumber() != null)
            ((EditText) editDialog.findViewById( R.id.number_input )).setText( String.valueOf( song.getNumber() ) );
        if (song.getLength() != null)
            ((EditText) editDialog.findViewById( R.id.length_input )).setText( String.valueOf( song.getLength() ) );

        return editDialog;
    }

    private Dialog buildSongDialog( String title, OnDismissListener dismissListener )
    {
        // initialize the dialog
        Dialog dialog = new Dialog( activity );
        dialog.setTitle( title );
        dialog.setContentView( R.layout.song_dialog );
        dialog.setOnDismissListener( dismissListener );

        return dialog;
    }
}