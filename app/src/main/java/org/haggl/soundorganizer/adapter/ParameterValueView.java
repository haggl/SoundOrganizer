/*
 * Copyright 2012-2014 Sebastian Neuser
 *
 * This file is part of SoundOrganizer.
 *
 * SoundOrganizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SoundOrganizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SoundOrganizer.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.haggl.soundorganizer.adapter;

import org.haggl.soundorganizer.SoundOrganizerApplication;
import org.haggl.soundorganizer.data.Setting;
import org.haggl.soundorganizer.database.DatabaseAssistant;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Extends EditText to hold information about the device and the parameter whose value it manages.
 *
 * @author haggl
 */
public class ParameterValueView extends EditText implements TextView.OnEditorActionListener
{
    // DEBUG variables
    private static final String TAG = ParameterValueView.class.getSimpleName();

    private final DatabaseAssistant assistant;
    private Setting setting;

    public ParameterValueView( Context context, AttributeSet attrs )
    {
        super( context, attrs );
        setOnEditorActionListener( this );
        assistant = DatabaseAssistant.getInstance();
    }

    public Setting getSetting()
    {
        return setting;
    }

    public void setSetting( Setting setting )
    {
        this.setting = setting;
    }

    @Override
    public boolean onEditorAction( TextView v, int actionId, KeyEvent event )
    {
        if (SoundOrganizerApplication.DEBUG) Log.d( TAG, "onEditorAction()" );

        // Update the setting
        ParameterValueView valueView = (ParameterValueView) v;
        String value = valueView.getText().toString();
        if (value.equals("")) {
            setting.setValue(null);
        }
        else {
            setting.setValue(valueView.getText().toString());
        }

        // Update database
        if (setting.getID() != null) {
            if (setting.getValue() == null) {
                assistant.destroySetting(setting.getID());
            }
            else {
                assistant.updateSetting(setting.getID(), setting.getValue());
            }
        }
        else if (setting.getValue() != null) {
            setting.setID(assistant.createSetting(setting.getSongID(),
                                                  setting.getDeviceID(),
                                                  setting.getName(),
                                                  setting.getValue()));
        }

        // Hide input method
        InputMethodManager imm = (InputMethodManager) SoundOrganizerApplication.getAppContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindowToken(), 0);

        return true;
    }
}
