/*
 * Copyright 2012-2014 Sebastian Neuser
 *
 * This file is part of SoundOrganizer.
 *
 * SoundOrganizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SoundOrganizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SoundOrganizer.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.haggl.soundorganizer.adapter;

import java.util.ArrayList;

import org.haggl.soundorganizer.R;
import org.haggl.soundorganizer.SoundOrganizerApplication;
import org.haggl.soundorganizer.data.Device;
import org.haggl.soundorganizer.data.Parameter;
import org.haggl.soundorganizer.data.Setting;
import org.haggl.soundorganizer.database.DatabaseAssistant;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * Inflates the list of devices and their settings for a song.
 * @author haggl
 */
public class DeviceListAdapter extends BaseExpandableListAdapter
{
    // DEBUG variables
    private static final String TAG = DeviceListAdapter.class.getSimpleName();

    // state variables
    private final ExpandableListView listView;
    private final LayoutInflater inflater;
    private final ArrayList<Device> devices;
    private final boolean[] initialized;
    private final long songID;
    private final Activity activity;

    public DeviceListAdapter( ExpandableListView listView, long songID, Activity activity ) {
        super();
        this.listView = listView;
        this.songID = songID;
        this.activity = activity;
        this.inflater = (LayoutInflater) SoundOrganizerApplication.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.devices = DatabaseAssistant.getInstance().listDevices(songID);
        this.initialized = new boolean[this.devices.size()];

        this.listView.setOnItemLongClickListener(new OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> parent, final View view, int position, long id) {
                if (SoundOrganizerApplication.DEBUG) {
                    Log.v(TAG, "-> onItemLongClick()");
                }

                // If a device name was clicked, show a delete dialog
                if (view.getClass().isAssignableFrom(LinearLayout.class)) {
                    Context ctx = DeviceListAdapter.this.listView.getContext();
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ctx);
                    dialogBuilder.setMessage("Delete settings for this device?");

                    dialogBuilder.setPositiveButton("Delete", new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Delete the relevant settings from the database
                            TextView deviceLabel = (TextView) view.findViewById( R.id.device_label );
                            String deviceName = deviceLabel.getText().toString();
                            DatabaseAssistant assistant = DatabaseAssistant.getInstance();
                            assistant.destroySettingsForSongIdAndDeviceName(DeviceListAdapter.this.songID,
                                                                            deviceName);

                            // Reload the activity
                            Activity activity = DeviceListAdapter.this.activity;
                            activity.finish();
                            activity.startActivity(activity.getIntent());
                        }
                    });

                    dialogBuilder.setNegativeButton("Cancel", new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    dialogBuilder.show();
                }
                return true;
            }
        });
    }

    @Override
    public boolean isEmpty()
    {
        return this.devices.isEmpty();
    }

    @Override
    public boolean hasStableIds()
    {
        return true;
    }

    @Override
    public Object getChild( int deviceNumber, int paramNumber )
    {
        Device device = this.devices.get( deviceNumber );
        return device.getSettings().get( paramNumber );
    }

    @Override
    public long getChildId( int deviceNumber, int paramNumber )
    {
        Device device = this.devices.get( deviceNumber );
        Long settingId = device.getSettings().get( paramNumber ).getID();
        return (settingId != null) ? settingId.longValue() : -1;
    }

    @Override
    public View getChildView( int deviceNumber, int paramNumber, boolean isLastChild, View convertView, ViewGroup parent )
    {
        if (SoundOrganizerApplication.DEBUG) Log.v( TAG, "-> getChildView()" );

        // find setting value if it exists
        Device device = this.devices.get( deviceNumber );
        Parameter parameter = DatabaseAssistant.getInstance().listParameters( device.getID() ).get( paramNumber );
        Setting setting = null;
        for (Setting currentSetting : device.getSettings())
            if (currentSetting.getName().equals( parameter.getName() ))
                setting = currentSetting;

        // if there is no setting in the database create a dummy
        if (setting == null)
        {
            setting = new Setting( null, this.songID, device.getID(), parameter.getName(), null );
            device.getSettings().add( setting );
        }

        // inflate and update views
        View parameterView = this.inflater.inflate( R.layout.parameter_item, null );
        ((TextView) parameterView.findViewById( R.id.param_label )).setText( parameter.getName() + " {" + parameter.getRange() + "}: " );

        ParameterValueView valueView = (ParameterValueView) parameterView.findViewById( R.id.param_value );
        valueView.setSetting( setting );
        if (setting.getValue() == null) valueView.setText( null );
        else valueView.setText( String.valueOf( setting.getValue() ) );

        return parameterView;
    }

    @Override
    public int getChildrenCount( int deviceNumber )
    {
        int count = DatabaseAssistant.getInstance().countParametersForDevice( this.devices.get( deviceNumber ).getID() );
        if (SoundOrganizerApplication.DEBUG) Log.v( TAG, "-> getChildrenCount(): " + count );
        return count;
    }

    @Override
    public Device getGroup( int deviceNumber )
    {
        return this.devices.get( deviceNumber );
    }

    @Override
    public int getGroupCount()
    {
        if (SoundOrganizerApplication.DEBUG) Log.v( TAG, "-> getGroupCount(): " + this.devices.size() );
        return this.devices.size();
    }

    @Override
    public long getGroupId( int deviceNumber )
    {
        return this.devices.get( deviceNumber ).getID();
    }

    @Override
    public View getGroupView( int deviceNumber, boolean isExpanded, View convertView, ViewGroup parent )
    {
        if (SoundOrganizerApplication.DEBUG) Log.v( TAG, "-> getGroupView()" );

        // get device
        Device currentDevice = this.devices.get( deviceNumber );

        // inflate the device
        View deviceView = this.inflater.inflate( R.layout.device_item, null );
        TextView deviceLabel = (TextView) deviceView.findViewById( R.id.device_label );
        deviceLabel.setText( currentDevice.getName() );

        // grey out devices for which no settings are stored, automatically expand the others
        if (currentDevice.getSettings().isEmpty()) deviceLabel.setTextColor( Color.DKGRAY );
        else if (this.initialized[deviceNumber] == false)
        {
            this.listView.expandGroup( deviceNumber );
            this.initialized[deviceNumber] = true;
        }
        return deviceView;
    }

    @Override
    public boolean isChildSelectable( int deviceNumber, int paramNumber )
    {
        return true;
    }
}
