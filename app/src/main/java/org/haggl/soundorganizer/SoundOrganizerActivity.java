/*
 * Copyright 2012-2014 Sebastian Neuser
 *
 * This file is part of SoundOrganizer.
 *
 * SoundOrganizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SoundOrganizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SoundOrganizer.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.haggl.soundorganizer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Formatter;

import org.haggl.soundorganizer.data.Song;
import org.haggl.soundorganizer.database.DatabaseAssistant;
import org.haggl.soundorganizer.database.Exporter;
import org.haggl.soundorganizer.database.Importer;
import org.haggl.soundorganizer.R;

import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Displays a list of all songs.
 *
 * @author haggl
 */
public class SoundOrganizerActivity extends ListActivity implements OnItemClickListener, OnItemLongClickListener
{
    // state variables
    private ArrayList<Song> songs;

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.sounds_layout );
        ListView list = (ListView) findViewById( android.R.id.list );
        list.setLongClickable( true );
        list.setOnItemClickListener( this );
        list.setOnItemLongClickListener( this );
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu )
    {
        getMenuInflater().inflate( R.menu.sounds_menu, menu );
        return true;
    }

    @Override
    public void onItemClick( AdapterView<?> adapter, View v, int position, long id )
    {
        Intent songIntent = new Intent( v.getContext(), SongActivity.class );
        songIntent.putExtra( "songID", songs.get( position ).getID() );
        startActivity( songIntent );
    }

    @Override
    public boolean onItemLongClick( AdapterView<?> adapter, View v, int position, long id )
    {
        // show a dialog
        new DialogFactory( this ).getEditDialog( songs.get( position ) ).show();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        switch (item.getItemId())
        {
            case R.id.add_song:
                // show a dialog
                new DialogFactory( this ).getAddDialog().show();
                break;

            case R.id.clear_set:
                DatabaseAssistant.getInstance().clearSetlist();
                for(Song currentSong : songs)
                    currentSong.setNumber( null );
                updateData();
                break;

            case R.id.import_data:
                Importer.importData();
                updateData();
                break;

            case R.id.export_data:
                Exporter.exportData();
                break;
        }
        return true;
    }

    @Override
    protected void onPause()
    {
        // close the database
        DatabaseAssistant.getInstance().close();
        super.onPause();
    }

    @Override
    protected void onResume()
    {
        // open the database and update the view
        DatabaseAssistant.getInstance().open();
        updateData();
        super.onResume();
    }

    public void updateData()
    {
        // update songs
        songs = DatabaseAssistant.getInstance().listSongs();
        Collections.sort( songs );

        // update songs list view
        setListAdapter( new ArrayAdapter<Song>( this, R.layout.song_item, songs ){
            @Override
            public View getView( int position, View convertView, ViewGroup parent )
            {
                // inflate layout
                Song currentSong = getItem( position );
                View songView = getLayoutInflater().inflate( R.layout.song_item, null );

                // fill in the data
                TextView titleView = (TextView) songView.findViewById( R.id.song_title );
                titleView.setText( currentSong.getTitle() );

                TextView lengthView = (TextView) songView.findViewById( R.id.song_length );
                if (currentSong.getLength() != null)
                {
                    Formatter formatter = new Formatter();
                    formatter.format( "(%02d:%02d)", new Object[]{ currentSong.getLength()/60, currentSong.getLength()%60 } );
                    lengthView.setText( formatter.toString() );
                    formatter.close();
                }

                TextView numberView = (TextView) songView.findViewById( R.id.song_number );
                if (currentSong.getNumber() == null)
                {
                    numberView.setText( "[+]" );
                    numberView.setTextColor( Color.DKGRAY );
                    titleView.setTextColor( Color.DKGRAY );
                    lengthView.setTextColor( Color.DKGRAY );
                }
                else numberView.setText( "[" + currentSong.getNumber() + "]" );

                return songView;
            }
        });

        // compute set length
        int sum = 0;
        for (Song currentSong : songs)
            if (currentSong.getNumber() != null) sum += currentSong.getLength();

        // update set length view
        String value = "n/a";
        if (sum > 0)
        {
            value = sum / 60 + ":";
            int seconds = sum % 60;
            if (seconds < 10) value += "0" + seconds;
            else value += seconds;
        }
        ((TextView) findViewById( R.id.set_value )).setText( value );
    }
}