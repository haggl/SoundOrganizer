/*
 * Copyright 2012-2014 Sebastian Neuser
 *
 * This file is part of SoundOrganizer.
 *
 * SoundOrganizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SoundOrganizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SoundOrganizer.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.haggl.soundorganizer.database;

import org.haggl.soundorganizer.SoundOrganizerApplication;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


/**
 * Initializes and opens the SQLite database.
 * @author haggl
 */
public class DatabaseManager extends SQLiteOpenHelper
{
    // DEBUG-variables
    private static final String TAG = DatabaseManager.class.getSimpleName();

    // database constants
    private static final String DATABASE_NAME = "sounds.db";
    private static final int DATABASE_VERSION = 12;

    // table names
    public static final String TABLE_DEVICES = "devices";
    public static final String TABLE_PARAMETERS = "params";
    public static final String TABLE_SETTINGS = "settings";
    public static final String TABLE_SONGS = "songs";

    // column names
    public static final String COLUMN_PARAM_ID = "parid";
    public static final String COLUMN_DEVICE_ID = "devid";
    public static final String COLUMN_SETTING_ID = "setid";
    public static final String COLUMN_SONG_ID = "sonid";

    public static final String COLUMN_DEVICE_NAME = "devname";
    public static final String COLUMN_LENGTH = "length";
    public static final String COLUMN_NUMBER = "number";
    public static final String COLUMN_PARAM_NAME = "parname";
    public static final String COLUMN_RANGE = "range";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_VALUE = "value";

    // column types
    private static final String TYPE_FOREIGN_KEY = " integer";
    private static final String TYPE_KEY = " integer primary key autoincrement";
    private static final String TYPE_NAME = " text not null";
    private static final String TYPE_NUMBER = " integer";
    private static final String TYPE_RANGE = " text not null";
    private static final String TYPE_VALUE = " text";

    // database creation sql statements
    private static final String CREATE_DEVICES = "CREATE TABLE " + TABLE_DEVICES
            + " ( " + COLUMN_DEVICE_ID + TYPE_KEY
            + " , " + COLUMN_DEVICE_NAME + TYPE_NAME
            + " );";
    private static final String CREATE_PARAMETERS = "CREATE TABLE " + TABLE_PARAMETERS
            + " ( " + COLUMN_PARAM_ID + TYPE_KEY
            + " , " + COLUMN_DEVICE_ID + TYPE_FOREIGN_KEY
            + " , " + COLUMN_PARAM_NAME + TYPE_NAME
            + " , " + COLUMN_RANGE + TYPE_RANGE
            + " , FOREIGN KEY (" + COLUMN_DEVICE_ID + ") REFERENCES " + TABLE_DEVICES + "(" + COLUMN_DEVICE_ID + ")"
            + " );";
    private static final String CREATE_SETTINGS = "CREATE TABLE " + TABLE_SETTINGS
            + " ( " + COLUMN_SETTING_ID + TYPE_KEY
            + " , " + COLUMN_SONG_ID + TYPE_FOREIGN_KEY
            + " , " + COLUMN_DEVICE_ID + TYPE_FOREIGN_KEY
            + " , " + COLUMN_PARAM_NAME + TYPE_NAME
            + " , " + COLUMN_VALUE + TYPE_VALUE
            + " , FOREIGN KEY (" + COLUMN_SONG_ID + ") REFERENCES " + TABLE_SONGS + "(" + COLUMN_SONG_ID + ")"
            + " , FOREIGN KEY (" + COLUMN_DEVICE_ID + ") REFERENCES " + TABLE_DEVICES + "(" + COLUMN_DEVICE_ID + ")"
            + " );";
    private static final String CREATE_SONGS = "CREATE TABLE " + TABLE_SONGS
            + " ( " + COLUMN_SONG_ID + TYPE_KEY
            + " , " + COLUMN_TITLE + TYPE_NAME
            + " , " + COLUMN_NUMBER + TYPE_NUMBER
            + " , " + COLUMN_LENGTH + TYPE_NUMBER
            + " );";

    public DatabaseManager( Context context )
    {
        super( context, DATABASE_NAME, null, DATABASE_VERSION );
    }

    @Override
    public void onCreate( SQLiteDatabase database )
    {
        if (SoundOrganizerApplication.DEBUG) Log.i( TAG, CREATE_SONGS );
        database.execSQL( CREATE_SONGS );

        if (SoundOrganizerApplication.DEBUG) Log.i( TAG, CREATE_DEVICES );
        database.execSQL( CREATE_DEVICES );

        if (SoundOrganizerApplication.DEBUG) Log.i( TAG, CREATE_PARAMETERS );
        database.execSQL( CREATE_PARAMETERS );

        if (SoundOrganizerApplication.DEBUG) Log.i( TAG, CREATE_SETTINGS );
        database.execSQL( CREATE_SETTINGS );
    }

    @Override
    public void onUpgrade( SQLiteDatabase db, int oldVersion, int newVersion )
    {
        Log.w( TAG, "upgrading database from version " + oldVersion + " to " + newVersion );
        db.execSQL( "DROP TABLE IF EXISTS " + TABLE_SETTINGS );
        db.execSQL( "DROP TABLE IF EXISTS " + TABLE_PARAMETERS );
        db.execSQL( "DROP TABLE IF EXISTS " + TABLE_DEVICES );
        db.execSQL( "DROP TABLE IF EXISTS " + TABLE_SONGS );
        onCreate( db );
    }
}
