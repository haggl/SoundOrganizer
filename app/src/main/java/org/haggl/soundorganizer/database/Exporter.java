/*
 * Copyright 2012-2014 Sebastian Neuser
 *
 * This file is part of SoundOrganizer.
 *
 * SoundOrganizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SoundOrganizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SoundOrganizer.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.haggl.soundorganizer.database;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Calendar;

import org.haggl.soundorganizer.SoundOrganizerApplication;

import android.database.Cursor;
import android.os.Environment;
import android.util.Log;

/**
 * Exports the current database to XML.
 *
 * @author haggl
 */
public class Exporter
{
    // DEBUG variables
    private static final String TAG = Exporter.class.getSimpleName();

    // XML tag definitions and snippets
    private static final String XML_VERSION = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    private static final String CLOSE_STARTING_TAG = "\">";
    private static final String CLOSE_TAG = "\" />";
    private static final String BEGIN_DB = "<sounds>";
    private static final String END_DB = "</sounds>";
    private static final String BEGIN_DEVICESPEC = "<devicespec name=\"";
    private static final String END_DEVICESPEC = "</devicespec>";
    private static final String BEGIN_PARAMSPEC = "<paramspec name=\"";
    private static final String PARAMSPEC_RANGE = "\" range=\"";
    private static final String BEGIN_SONG = "<song name=\"";
    private static final String SONG_LENGTH = "\" length=\"";
    private static final String SONG_NUMBER = "\" number=\"";
    private static final String END_SONG = "</song>";
    private static final String BEGIN_DEVICE = "<device name=\"";
    private static final String END_DEVICE = "</device>";
    private static final String BEGIN_SETTING = "<setting name=\"";
    private static final String SETTING_VALUE = "\" value=\"";
    private static final String INDENT1 = "\t";
    private static final String INDENT2 = "\t\t";
    private static final String INDENT3 = "\t\t\t";

    // SQL snippets
    private static final String FROM = " FROM ";
    private static final String NATURAL_JOIN = " NATURAL JOIN ";
    private static final String SELECT = "SELECT ";

    // table and column name shortcuts
    private static final String DEVICES = DatabaseManager.TABLE_DEVICES;
    private static final String PARAMS = DatabaseManager.TABLE_PARAMETERS;
    private static final String SETTINGS = DatabaseManager.TABLE_SETTINGS;
    private static final String SONGS = DatabaseManager.TABLE_SONGS;

    private static final String DEVICE_NAME = DEVICES + "." + DatabaseManager.COLUMN_DEVICE_NAME;
    private static final String LENGTH = SONGS + "." + DatabaseManager.COLUMN_LENGTH;
    private static final String NUMBER = SONGS + "." + DatabaseManager.COLUMN_NUMBER;
    private static final String PARAM_NAME = PARAMS + "." + DatabaseManager.COLUMN_PARAM_NAME;
    private static final String RANGE = PARAMS + "." + DatabaseManager.COLUMN_RANGE;
    private static final String SETTING_NAME = SETTINGS + "." + DatabaseManager.COLUMN_PARAM_NAME;
    private static final String TITLE = SONGS + "." + DatabaseManager.COLUMN_TITLE;
    private static final String VALUE = SETTINGS + "." + DatabaseManager.COLUMN_VALUE;

    // state variables
    private static PrintWriter output;
    private static String statement;
    private static Cursor cursor;
    private static String currentSong;
    private static String currentDevice;

    private Exporter() {}

    public static void exportData()
    {
        try
        {
            // create filename according to current date
            Calendar today = Calendar.getInstance();
            String year = String.valueOf( today.get( Calendar.YEAR ) );
            String month = String.valueOf( today.get( Calendar.MONTH )+1 );
            if (Integer.valueOf( month ) < 10 ) month = "0" + month;
            String day = String.valueOf( today.get( Calendar.DAY_OF_MONTH ) );
            if (Integer.valueOf( day ) < 10 ) day = "0" + day;
            String filePath = Environment.getExternalStorageDirectory().toString();
            filePath += "/" + SoundOrganizerApplication.exportFileName;
            filePath += "-" + year + month + day + ".xml";

            // delete the file if it exists and open an output stream
            if (SoundOrganizerApplication.DEBUG) Log.d( TAG, "opening file for output: " + filePath );
            File outputFile = new File( filePath );
            output = new PrintWriter( new FileOutputStream( outputFile ) );

            // begin export
            output.println( XML_VERSION );
            output.println( BEGIN_DB );

            // query device specifications
            statement = SELECT + DEVICE_NAME + "," + PARAM_NAME + "," + RANGE + FROM + DEVICES + NATURAL_JOIN + PARAMS;
            if (SoundOrganizerApplication.DEBUG) Log.d( TAG, statement );
            cursor = DatabaseAssistant.getInstance().query( statement );

            // write device specifications to file
            cursor.moveToFirst();
            while (!cursor.isAfterLast())
            {
                // update device
                currentDevice = cursor.getString( 0 );

                if (SoundOrganizerApplication.DEBUG)
                {
                    Log.v( TAG, INDENT1 + BEGIN_DEVICESPEC + currentDevice + CLOSE_STARTING_TAG );
                    Log.v( TAG, INDENT2 + BEGIN_PARAMSPEC + cursor.getString( 1 ) + PARAMSPEC_RANGE + cursor.getString( 2 ) + CLOSE_TAG );
                }

                // print the device and it's first parameter
                output.println( INDENT1 + BEGIN_DEVICESPEC + currentDevice + CLOSE_STARTING_TAG );
                output.println( INDENT2 + BEGIN_PARAMSPEC + cursor.getString( 1 ) + PARAMSPEC_RANGE + cursor.getString( 2 ) + CLOSE_TAG );

                // print the device's parameters
                while (cursor.moveToNext() && cursor.getString( 0 ).equals( currentDevice ))
                {
                    if (SoundOrganizerApplication.DEBUG) Log.v( TAG, INDENT2 + BEGIN_PARAMSPEC + cursor.getString( 1 ) + PARAMSPEC_RANGE + cursor.getString( 2 ) + CLOSE_TAG );
                    output.println( INDENT2 + BEGIN_PARAMSPEC + cursor.getString( 1 ) + PARAMSPEC_RANGE + cursor.getString( 2 ) + CLOSE_TAG );
                }

                // print end tag
                if (SoundOrganizerApplication.DEBUG) Log.v( TAG, INDENT1 + END_DEVICESPEC );
                output.println( INDENT1 + END_DEVICESPEC );
            }
            cursor.close();

            // query songs and settings
            statement = SELECT + TITLE + "," + LENGTH + "," + NUMBER + "," + DEVICE_NAME + "," + SETTING_NAME + "," + VALUE
                      + FROM + SONGS + NATURAL_JOIN + DEVICES + NATURAL_JOIN + SETTINGS ;
            if (SoundOrganizerApplication.DEBUG) Log.d( TAG, statement );
            cursor = DatabaseAssistant.getInstance().query( statement );

            // write device settings
            cursor.moveToFirst();
            while (!cursor.isAfterLast())
            {
                // update song and device
                currentSong = cursor.getString( 0 );
                currentDevice = cursor.getString( 3 );

                if (SoundOrganizerApplication.DEBUG)
                {
                    Log.v( TAG, INDENT1 + BEGIN_SONG + currentSong + CLOSE_STARTING_TAG );
                }

                // print the song
                if (cursor.isNull( 1 )) // length not specified
                    if (cursor.isNull( 2 )) // number not specified
                        output.println( INDENT1 + BEGIN_SONG + currentSong + CLOSE_STARTING_TAG );
                    else
                        output.println( INDENT1 + BEGIN_SONG + currentSong + SONG_NUMBER + cursor.getShort( 2 ) + CLOSE_STARTING_TAG );
                else
                    if (cursor.isNull( 2 )) // number not specified
                        output.println( INDENT1 + BEGIN_SONG + currentSong + SONG_LENGTH + cursor.getShort( 1 ) + CLOSE_STARTING_TAG );
                    else
                        output.println( INDENT1 + BEGIN_SONG + currentSong + SONG_LENGTH + cursor.getShort( 1 ) + SONG_NUMBER + cursor.getShort( 2 ) + CLOSE_STARTING_TAG );

                // rewind the cursor one step
                cursor.moveToPrevious();

                while (cursor.moveToNext() && cursor.getString( 0 ).equals( currentSong ))
                {
                    // update device
                    currentDevice = cursor.getString( 3 );

                    if (SoundOrganizerApplication.DEBUG)
                    {
                        Log.v( TAG, INDENT2 + BEGIN_DEVICE + currentDevice + CLOSE_STARTING_TAG );
                        Log.v( TAG, INDENT3 + BEGIN_SETTING + cursor.getString( 4 ) + SETTING_VALUE + cursor.getString( 5 ) + CLOSE_TAG );
                    }

                    // print the device and it's first setting
                    output.println( INDENT2 + BEGIN_DEVICE + currentDevice + CLOSE_STARTING_TAG );
                    output.println( INDENT3 + BEGIN_SETTING + cursor.getString( 4 ) + SETTING_VALUE + cursor.getString( 5 ) + CLOSE_TAG );

                    // print further device settings
                    while (cursor.moveToNext() && cursor.getString( 0 ).equals( currentSong ) && cursor.getString( 3 ).equals( currentDevice ))
                    {
                        if (SoundOrganizerApplication.DEBUG) Log.v( TAG, INDENT3 + BEGIN_SETTING + cursor.getString( 4 ) + SETTING_VALUE + cursor.getString( 5 ) + CLOSE_TAG );
                        output.println( INDENT3 + BEGIN_SETTING + cursor.getString( 4 ) + SETTING_VALUE + cursor.getString( 5 ) + CLOSE_TAG );
                    }

                    // print the device end-tag and rewind the cursor one step
                    if (SoundOrganizerApplication.DEBUG) Log.v( TAG, INDENT2 + END_DEVICE );
                    output.println( INDENT2 + END_DEVICE );
                    cursor.moveToPrevious();
                }

                if (SoundOrganizerApplication.DEBUG) Log.v( TAG, INDENT1 + END_SONG );
                output.println( INDENT1 + END_SONG );
            }
            cursor.close();

            // end export and close file
            output.println( END_DB );
            output.close();
        }
        catch (FileNotFoundException e)
        {
            Log.e( TAG, "File not found: " + e.getMessage() );
        }
    }
}
