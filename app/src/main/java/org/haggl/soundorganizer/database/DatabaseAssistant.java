/*
 * Copyright 2012-2014 Sebastian Neuser
 *
 * This file is part of SoundOrganizer.
 *
 * SoundOrganizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SoundOrganizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SoundOrganizer.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.haggl.soundorganizer.database;

import java.util.ArrayList;

import org.haggl.soundorganizer.SoundOrganizerApplication;
import org.haggl.soundorganizer.data.Device;
import org.haggl.soundorganizer.data.Parameter;
import org.haggl.soundorganizer.data.Setting;
import org.haggl.soundorganizer.data.Song;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


/**
 * Provides helper functions for database operations.
 * @author haggl
 */
public class DatabaseAssistant
{
    // DEBUG variables
    private static final String TAG = DatabaseAssistant.class.getSimpleName();

    // Database variables
    private SQLiteDatabase database;
    private final DatabaseManager helper;

    // Singleton
    public static final DatabaseAssistant INSTANCE = new DatabaseAssistant();

    public static DatabaseAssistant getInstance() {
        return INSTANCE;
    }

    private DatabaseAssistant() {
        helper = new DatabaseManager(SoundOrganizerApplication.getAppContext());
    }

    public void open() throws SQLException {
        database = helper.getWritableDatabase();
    }

    public Cursor query(String statement) {
        return database.rawQuery(statement, null);
    }

    public void close() {
        helper.close();
    }

    public void wipe() {
        database.delete(DatabaseManager.TABLE_SETTINGS, null, null);
        database.delete(DatabaseManager.TABLE_PARAMETERS, null, null);
        database.delete(DatabaseManager.TABLE_DEVICES, null, null);
        database.delete(DatabaseManager.TABLE_SONGS, null, null);
    }

    public void clearSetlist() {
        ContentValues values = new ContentValues();
        values.putNull("number");
        database.update(DatabaseManager.TABLE_SONGS, values, null, null);
    }

    public long createSong(String name, Short number, Short length) {
        ContentValues values = new ContentValues();
        values.put(DatabaseManager.COLUMN_TITLE, name);
        values.put(DatabaseManager.COLUMN_NUMBER, number);
        values.put(DatabaseManager.COLUMN_LENGTH, length);
        long id = database.insert(DatabaseManager.TABLE_SONGS, null, values);

        if (SoundOrganizerApplication.DEBUG) {
            Cursor cursor = database.query(DatabaseManager.TABLE_SONGS,
                                           new String[] { DatabaseManager.COLUMN_TITLE,
                                                          DatabaseManager.COLUMN_NUMBER,
                                                          DatabaseManager.COLUMN_LENGTH },
                                           DatabaseManager.COLUMN_SONG_ID + " = " + id,
                                           null, null, null, null);

            cursor.moveToFirst();
            Log.d(TAG, "inserted song: name=" + cursor.getString(0) + ", number=" + cursor.getShort(1) + ", length="
                    + cursor.getShort(2));
            cursor.close();
        }

        return id;
    }

    public Song readSong(long songID) {
        if (SoundOrganizerApplication.DEBUG) {
            Log.v(TAG, "-> getSong( " + songID + " )");
        }
        Cursor cursor = database.query(DatabaseManager.TABLE_SONGS,
                                       null,
                                       DatabaseManager.COLUMN_SONG_ID + " = " + songID,
                                       null, null, null, null);

        Song result = null;
        if (cursor.moveToFirst()) {
            if (cursor.isNull(2)) {
                if (cursor.isNull(3)) {
                    result = new Song(cursor.getLong(0), cursor.getString(1), null, null);
                }
                else {
                    result = new Song(cursor.getLong(0), cursor.getString(1), null, cursor.getShort(3));
                }
            }
            else if (cursor.isNull(3)) {
                result = new Song(cursor.getLong(0), cursor.getString(1), cursor.getShort(2), null);
            }
            else {
                result = new Song(cursor.getLong(0), cursor.getString(1), cursor.getShort(2), cursor.getShort(3));
            }
        }

        cursor.close();
        return result;
    }

    public void updateSong(Song song) {
        if (SoundOrganizerApplication.DEBUG) {
            Log.v(TAG, "-> updateSong( " + song.getID() + ", " + song.getTitle() + ", " + song.getNumber() + " )");
        }
        ContentValues values = new ContentValues();
        values.put(DatabaseManager.COLUMN_TITLE, song.getTitle());
        values.put(DatabaseManager.COLUMN_NUMBER, song.getNumber());
        values.put(DatabaseManager.COLUMN_LENGTH, song.getLength());

        database.update(DatabaseManager.TABLE_SONGS,
                        values,
                        DatabaseManager.COLUMN_SONG_ID + " = " + song.getID(),
                        null);
    }

    public int countSongsWithNumbers() {
        // Prepare statement
        String query = "SELECT COUNT(*) FROM " + DatabaseManager.TABLE_SONGS +
                      " WHERE " + DatabaseManager.COLUMN_NUMBER + " IS NOT NULL";

        // Execute and return
        Cursor cursor = database.rawQuery(query, null);
        cursor.moveToFirst();
        int count = cursor.getInt(0);
        cursor.close();
        return count;
    }

    public Long findIdForSongWithNumber(short number) {
        if (SoundOrganizerApplication.DEBUG) {
            Log.v(TAG, "-> findSongID( " + number + " )");
        }
        Cursor cursor = database.query(DatabaseManager.TABLE_SONGS,
                                       new String[] { DatabaseManager.COLUMN_SONG_ID },
                                       DatabaseManager.COLUMN_NUMBER + " = " + number,
                                       null, null, null, null);

        long id = -1;
        if (cursor.moveToFirst()) {
            id = cursor.getLong(0);
        }

        cursor.close();
        return id;
    }

    public ArrayList<Song> listSongs() {
        if (SoundOrganizerApplication.DEBUG) {
            Log.v(TAG, "-> listSongs()");
        }
        ArrayList<Song> result = new ArrayList<Song>();

        Cursor cursor = database.query(DatabaseManager.TABLE_SONGS, null, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            if (cursor.isNull(2)) {
                if (cursor.isNull(3)) {
                    result.add(new Song(cursor.getLong(0), cursor.getString(1), null, null));
                }
                else {
                    result.add(new Song(cursor.getLong(0), cursor.getString(1), null, cursor.getShort(3)));
                }
            }
            else if (cursor.isNull(3)) {
                result.add(new Song(cursor.getLong(0), cursor.getString(1), cursor.getShort(2), null));
            }
            else {
                result.add(new Song(cursor.getLong(0), cursor.getString(1), cursor.getShort(2), cursor.getShort(3)));
            }
            cursor.moveToNext();
        }

        // Close the cursor and return the result
        cursor.close();
        if (SoundOrganizerApplication.DEBUG) {
            Log.v(TAG, result.toString());
        }
        return result;
    }

    public long createDevice(String name) {
        ContentValues values = new ContentValues();
        values.put(DatabaseManager.COLUMN_DEVICE_NAME, name);
        long id = database.insert(DatabaseManager.TABLE_DEVICES, null, values);

        if (SoundOrganizerApplication.DEBUG) {
            Cursor cursor = database.query(DatabaseManager.TABLE_DEVICES,
                                           new String[] { DatabaseManager.COLUMN_DEVICE_NAME },
                                           DatabaseManager.COLUMN_DEVICE_ID + " = " + id,
                                           null, null, null, null);

            cursor.moveToFirst();
            Log.d(TAG, "inserted device: name=" + cursor.getString(0));
            cursor.close();
        }

        return id;
    }

    public Long findIdForDeviceWithName(String name) {
        if (SoundOrganizerApplication.DEBUG) {
            Log.v(TAG, "-> findDeviceID( \"" + name + "\" )");
        }
        Cursor cursor = database.query(DatabaseManager.TABLE_DEVICES,
                                       new String[] { DatabaseManager.COLUMN_DEVICE_ID },
                                       DatabaseManager.COLUMN_DEVICE_NAME + " = '" + name + "'",
                                       null, null, null, null);
        cursor.moveToFirst();
        long id = cursor.getLong(0);
        cursor.close();
        return id;
    }

    public ArrayList<Device> listDevices(long songID) {
        if (SoundOrganizerApplication.DEBUG) {
            Log.v(TAG, "-> listDevices( " + songID + " )");
        }
        ArrayList<Device> result = new ArrayList<Device>();

        Cursor cursor = database.query(DatabaseManager.TABLE_DEVICES,
                                       new String[] { DatabaseManager.COLUMN_DEVICE_ID,
                                                      DatabaseManager.COLUMN_DEVICE_NAME },
                                       null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ArrayList<Setting> params = listSettingsWithSongIdAndDevice(songID, cursor.getLong(0));
            Device device = new Device(cursor.getLong(0), cursor.getString(1), params);
            result.add(device);
            cursor.moveToNext();
        }

        // Close the cursor and return the result
        cursor.close();
        if (SoundOrganizerApplication.DEBUG) {
            Log.v(TAG, result.toString());
        }
        return result;
    }

    public long createParameter(long deviceID, String name, String range) {
        ContentValues values = new ContentValues();
        values.put(DatabaseManager.COLUMN_DEVICE_ID, deviceID);
        values.put(DatabaseManager.COLUMN_PARAM_NAME, name);
        values.put(DatabaseManager.COLUMN_RANGE, range);
        long id = database.insert(DatabaseManager.TABLE_PARAMETERS, null, values);

        if (SoundOrganizerApplication.DEBUG) {
            Cursor cursor = database.query(DatabaseManager.TABLE_PARAMETERS,
                                           new String[] { DatabaseManager.COLUMN_DEVICE_ID,
                                                          DatabaseManager.COLUMN_PARAM_NAME,
                                                          DatabaseManager.COLUMN_RANGE },
                                           DatabaseManager.COLUMN_PARAM_ID + " = " + id,
                                           null, null, null, null);

            cursor.moveToFirst();
            Log.d(TAG, "inserted parameter: deviceID=" + cursor.getLong(0) + ", name=" + cursor.getString(1)
                    + ", range=" + cursor.getString(2));
            cursor.close();
        }

        return id;
    }

    public int countParametersForDevice(long deviceID) {
        // Prepare statement
        String query = "SELECT COUNT(*) FROM " + DatabaseManager.TABLE_PARAMETERS;
        query += " WHERE " + DatabaseManager.COLUMN_DEVICE_ID + " = " + deviceID;

        // Execute and return
        Cursor cursor = database.rawQuery(query, null);
        cursor.moveToFirst();
        int count = cursor.getInt(0);
        cursor.close();
        return count;
    }

    public ArrayList<Parameter> listParameters(long deviceID) {
        if (SoundOrganizerApplication.DEBUG) {
            Log.v(TAG, "-> listParameters( " + deviceID + " )");
        }
        ArrayList<Parameter> result = new ArrayList<Parameter>();

        Cursor cursor = database.query(DatabaseManager.TABLE_PARAMETERS,
                                       new String[] { DatabaseManager.COLUMN_PARAM_ID,
                                                      DatabaseManager.COLUMN_PARAM_NAME,
                                                      DatabaseManager.COLUMN_RANGE },
                                       DatabaseManager.COLUMN_DEVICE_ID + " = " + deviceID,
                                       null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            result.add(new Parameter(cursor.getLong(0), cursor.getString(1), cursor.getString(2)));
            cursor.moveToNext();
        }

        // Close the cursor and return the result
        cursor.close();
        if (SoundOrganizerApplication.DEBUG) {
            Log.v(TAG, result.toString());
        }
        return result;

    }

    public long createSetting(Long songID, Long deviceID, String name, String value) {
        ContentValues values = new ContentValues();
        values.put(DatabaseManager.COLUMN_SONG_ID, songID);
        values.put(DatabaseManager.COLUMN_DEVICE_ID, deviceID);
        values.put(DatabaseManager.COLUMN_PARAM_NAME, name);
        values.put(DatabaseManager.COLUMN_VALUE, value);
        long id = database.insert(DatabaseManager.TABLE_SETTINGS, null, values);

        if (SoundOrganizerApplication.DEBUG) {
            Cursor cursor = database.query(DatabaseManager.TABLE_SETTINGS,
                                           new String[] { DatabaseManager.COLUMN_SONG_ID,
                                                          DatabaseManager.COLUMN_DEVICE_ID,
                                                          DatabaseManager.COLUMN_PARAM_NAME,
                                                          DatabaseManager.COLUMN_VALUE },
                                           DatabaseManager.COLUMN_SETTING_ID + " = " + id,
                                           null, null, null, null);

            cursor.moveToFirst();
            Log.d(TAG, "inserted setting: songID=" + cursor.getLong(0)
                                   + ", deviceID=" + cursor.getLong(1)
                                       + ", name=" + cursor.getString(2)
                                      + ", value=" + cursor.getShort(3));
            cursor.close();
        }

        return id;
    }

    public void updateSetting(long settingID, String value) {
        if (SoundOrganizerApplication.DEBUG) {
            Log.v(TAG, "-> updateSetting( " + settingID + ", \"" + value + "\" )");
        }
        ContentValues values = new ContentValues();
        values.put(DatabaseManager.COLUMN_VALUE, value);

        database.update(DatabaseManager.TABLE_SETTINGS,
                        values,
                        DatabaseManager.COLUMN_SETTING_ID + " = " + settingID,
                        null);
    }

    public void destroySetting(long settingID) {
        if (SoundOrganizerApplication.DEBUG) {
            Log.v(TAG, "-> destroySetting(" + settingID + ")");
        }

        database.delete(DatabaseManager.TABLE_SETTINGS,
                        DatabaseManager.COLUMN_SETTING_ID + " = " + settingID,
                        null);
    }

    public void destroySettingsForSongIdAndDeviceName(long songId, String deviceName) {
        if (SoundOrganizerApplication.DEBUG) {
            Log.v(TAG, "-> destroySettingsForSongIdAndDeviceName(" + songId + ", " + deviceName + ")");
        }

        long deviceId = findIdForDeviceWithName(deviceName);
        database.delete(DatabaseManager.TABLE_SETTINGS,
                        DatabaseManager.COLUMN_DEVICE_ID + " = " + deviceId + " and " +
                        DatabaseManager.COLUMN_SONG_ID + " = " + songId,
                        null);
    }

    public ArrayList<Setting> listSettingsWithSongIdAndDevice(long songID, long deviceID) {
        if (SoundOrganizerApplication.DEBUG) {
            Log.v(TAG, "-> listSettings( " + songID + ", " + deviceID + " )");
        }
        ArrayList<Setting> result = new ArrayList<Setting>();

        Cursor cursor = database.query(DatabaseManager.TABLE_SETTINGS,
                                       null,
                                       DatabaseManager.COLUMN_SONG_ID + " = " + songID +
                                       " and " +
                                       DatabaseManager.COLUMN_DEVICE_ID + " = " + deviceID,
                                       null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            result.add(new Setting(cursor.getLong(0),
                                   cursor.getLong(1),
                                   cursor.getLong(2),
                                   cursor.getString(3),
                                   cursor.getString(4)));
            cursor.moveToNext();
        }

        // Close the cursor and return the result
        cursor.close();
        if (SoundOrganizerApplication.DEBUG) {
            Log.v(TAG, result.toString());
        }
        return result;
    }
}
