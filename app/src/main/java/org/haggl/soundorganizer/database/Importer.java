/*
 * Copyright 2012-2014 Sebastian Neuser
 *
 * This file is part of SoundOrganizer.
 *
 * SoundOrganizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SoundOrganizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SoundOrganizer.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.haggl.soundorganizer.database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.SAXParserFactory;

import org.haggl.soundorganizer.SoundOrganizerApplication;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.os.Environment;
import android.util.Log;


/**
 * Imports an XML database file.
 * @author haggl
 */
public class Importer extends DefaultHandler
{
    // DEBUG variables
    private static final String TAG = Importer.class.getSimpleName();

    // state variables
    private Long currentSong;
    private Long currentDevice;

    // hide constructor
    private Importer() {}


    /**
     * Parses sounds.xml and fills the database.
     */
    public static void importData()
    {
        try
        {
            // parse the database file
            String filePath = Environment.getExternalStorageDirectory().toString();
            filePath += "/" + SoundOrganizerApplication.importFileName + ".xml";
            InputStream file = new FileInputStream( new File( filePath ) );
            SAXParserFactory.newInstance().newSAXParser().parse( file, new Importer() );
            file.close();
        }
        catch (FileNotFoundException e)
        {
            Log.e( TAG, "File not found: " + e.getMessage() );
        }
        catch (IOException e)
        {
            Log.e( TAG, "I/O error while trying to read file: " + e.getMessage() );
        }
        catch (Exception e)
        {
            Log.e( TAG, "Error parsing file: " + e.getMessage() );
        }
    }

    @Override
    public void startElement( String uri, String localName, String qName, Attributes attributes ) throws SAXException
    {
        if (SoundOrganizerApplication.DEBUG) Log.v( TAG, "-> startElement() <" + qName + " name=\"" + attributes.getValue( "name" ) + "\">" );

        if (qName.equals( "sounds" ))
            DatabaseAssistant.getInstance().wipe();

        if (qName.equals( "devicespec" ))
            currentDevice = DatabaseAssistant.getInstance().createDevice( attributes.getValue( "name" ) );

        if (qName.equals( "paramspec" ))
            DatabaseAssistant.getInstance().createParameter( currentDevice, attributes.getValue( "name" ), attributes.getValue( "range" ) );

        if (qName.equals( "song" ))
            if (attributes.getValue( "number" ) == null)
                if (attributes.getValue( "length" ) == null)
                    currentSong = DatabaseAssistant.getInstance().createSong( attributes.getValue( "name" ), null, null );
                else
                    currentSong = DatabaseAssistant.getInstance().createSong( attributes.getValue( "name" ), null, Short.parseShort( attributes.getValue( "length" ) ) );
            else
                if (attributes.getValue( "length" ) == null)
                    currentSong = DatabaseAssistant.getInstance().createSong( attributes.getValue( "name" ), Short.parseShort( attributes.getValue( "number" ) ), null );
                else
                    currentSong = DatabaseAssistant.getInstance().createSong( attributes.getValue( "name" ), Short.parseShort( attributes.getValue( "number" ) ), Short.parseShort( attributes.getValue( "length") ) );

        if (qName.equals( "device" ))
            currentDevice = DatabaseAssistant.getInstance().findIdForDeviceWithName( attributes.getValue( "name" ) );

        if (qName.equals( "setting" ))
            DatabaseAssistant.getInstance().createSetting( currentSong, currentDevice, attributes.getValue( "name" ), attributes.getValue( "value" ) );
    }

    @Override
    public void endElement( String uri, String localName, String qName ) throws SAXException
    {
        if (SoundOrganizerApplication.DEBUG) Log.v( TAG, "-> endElement() <" + qName + ">" );

        if (qName.equals( "device" ))
            currentDevice = null;

        if (qName.equals( "song" ))
            currentSong = null;

        if (qName.equals( "device" ))
            currentDevice = null;
    }
}